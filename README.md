# Poll

Poll is a hermes plugin for simple poll creation.

## Installation

- Drop the plugin inside de plugin folder.
- Run Hermes and generate configuration.
- Re-run Hermes to apply changes.

## Usage

```
!poll <question>
<answer 1>
...
<answer x>
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Author

- YahuJulie

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)