package fr.blueberry.studio.hermes.plugins.poll.commands;

import java.awt.Color;

import org.simpleyaml.configuration.file.YamlFile;

import fr.blueberry.studio.hermes.api.app.Sender;
import fr.blueberry.studio.hermes.api.bots.Bot;
import fr.blueberry.studio.hermes.api.bots.BotManager;
import fr.blueberry.studio.hermes.api.commands.Command;
import fr.blueberry.studio.hermes.api.utils.CommandHelper;
import fr.blueberry.studio.hermes.api.utils.MessageEmbedHelper;
import fr.blueberry.studio.hermes.api.utils.StringHelper;
import fr.blueberry.studio.hermes.plugins.poll.Poll;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;

public class CommandPoll extends Command {
    private YamlFile config = Poll.INSTANCE.getConfiguration();
    private int maxChoice = config.getInt("options.maxChoice");

    @Override
    public void execute(Sender sender, String[] args) {
        if(sender.getUser() != null){
            if(sender.hasPermission(config.getString("permission.create"))){
                if(args.length > 0) {
                    String msg = StringHelper.stringify(0, args);
                    String[] content = msg.split(System.getProperty("line.separator"));
                    
                    if(content.length > maxChoice+1){
                        sender.reply(MessageEmbedHelper.fastEmbed("Tu as dépassé la limite de réponse possible. (Nombre maximum : "+ String.valueOf(maxChoice) +" )"));
                    } else {
                        int r = config.getInt("message.color.r");
                        int g = config.getInt("message.color.g");
                        int b = config.getInt("message.color.b");
                        String type = config.getString("options.format");
                        User senderUser = sender.getUser();
                        BotManager botManager = getHermes().getBotManager();
                        Bot bot = botManager.pickRandomBot(true);
    
                        EmbedBuilder eb = new EmbedBuilder();
                        eb.setAuthor(senderUser.getName(), config.getString("message.url"), senderUser.getEffectiveAvatarUrl());
                        eb.setColor(new Color(r, g, b));
    
                        for (int index = 0; index < content.length; index++) {
                            switch (index) {
                                case 0:
                                    eb.setTitle(content[index]);
                                    break;
                                case 1:
                                    eb.appendDescription(config.getString("options." + type + ".a") + " " + content[index] + "\n");
                                    break;
                                case 2:
                                    eb.appendDescription(config.getString("options." + type + ".b") + " " + content[index] + "\n");
                                    break;
                                case 3:
                                    eb.appendDescription(config.getString("options." + type + ".c") + " " + content[index] + "\n");
                                    break;
                                case 4:
                                    eb.appendDescription(config.getString("options." + type + ".d") + " " + content[index] + "\n");
                                    break;
                                case 5:
                                    eb.appendDescription(config.getString("options." + type + ".e") + " " + content[index] + "\n");
                                    break;
                            }
                        }
                        String messageChannel = sender.getMessageChannel().getId();
                        bot.getJDA().getTextChannelById(messageChannel).sendMessage(eb.build()).queue(message -> {
                            for (int i = 0; i < content.length; i++) {
                                switch (i) {
                                    case 0:
                                        break;
                                    case 1:
                                        message.addReaction(config.getString("options." + type + ".a")).queue();
                                        break;
                                    case 2:
                                        message.addReaction(config.getString("options." + type + ".b")).queue();
                                        break;
                                    case 3:
                                        message.addReaction(config.getString("options." + type + ".c")).queue();
                                        break;
                                    case 4:
                                        message.addReaction(config.getString("options." + type + ".d")).queue();
                                        break;
                                    case 5:
                                        message.addReaction(config.getString("options." + type + ".e")).queue();
                                        break;
                                }
                            }
                        });
                    }
                } else {
                    CommandHelper.sendCommandWrongArgs(sender);
                }
            } else {
                sender.reply(MessageEmbedHelper.fastEmbed("Désolé mais tu n'as pas la permission d'executer cette commande"));
            }
        } else {
            getHermes().getLogger().error("Impossible de faire cette commande sans être un membre. ( bouuuuh la console bouuuuuh )");
        }
    }

    @Override
    public String getLabel() {
        return "poll";
    }

    @Override
    public String[] getAliases() {
        return new String[] {"p", "sondage"};
    }

    @Override
    public String getDescription() {
        return "Créer un sondage";
    }

    @Override
    public String getUsage() {
        return  """
                    Jusqu'à 5 réponses possible ( Au minimum 1 question et 1 réponse )
                    !poll <question>
                    <réponse 1>
                    <réponse 2>
                    <réponse 3>
                    <réponse 4>
                    <réponse 5>
                """;
    }

    @Override
    public boolean isOpRestricted() {
        return false;
    }
}
