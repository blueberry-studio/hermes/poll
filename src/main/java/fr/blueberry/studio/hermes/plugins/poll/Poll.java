package fr.blueberry.studio.hermes.plugins.poll;

import fr.blueberry.studio.hermes.api.plugins.Plugin;
import fr.blueberry.studio.hermes.plugins.poll.commands.CommandPoll;

public class Poll extends Plugin {
    public static Poll INSTANCE;
    
    @Override
    public void onLoad() {
        INSTANCE = this;
    }

    @Override
    public void onEnable() {
        getCommandRegistry().registerCommand(new CommandPoll(), this);
    }
}
